package com.keagan.prac.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.keagan.prac.model.*;
import com.keagan.prac.service.TracksService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SongsController.class)
public class SongsControllerTest {

    private final String URL = "/api/music";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TracksService tracksService;

    List<Song> testTracks;

    @BeforeEach
    void setup() {
        testTracks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Song s = new Song("track" + i, "artist" + i, "album" + i);
            testTracks.add(s);
        }
    }

    @Test
    void getTracks_None_ReturnsTracks() throws Exception {
        when(tracksService.getTracks()).thenReturn(new TrackList(testTracks));

        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tracks", hasSize(5)));
    }

    @Test
    void getTracks_None_ReturnsNoContent() throws Exception {
        when(tracksService.getTracks()).thenReturn(null);

        mockMvc.perform(get(URL))
                .andExpect(status().isNoContent());
    }

    @Test
    void getTracks_Title_ReturnsTracks() throws Exception {
        when(tracksService.getTracks(anyString(), anyString()))
                .thenReturn(new TrackList(testTracks));

        mockMvc.perform(get(URL + "?title=track"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tracks", hasSize(5)));
    }

    @Test
    void getTracks_Artist_ReturnsTracks() throws Exception {
        when(tracksService.getTracks(anyString(), anyString()))
                .thenReturn(new TrackList(testTracks));

        mockMvc.perform(get(URL + "?artist=artist"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tracks", hasSize(5)));
    }

    @Test
    void getTracks_TitleAndArtist_ReturnsTracks() throws Exception {
        when(tracksService.getTracks(anyString(), anyString()))
                .thenReturn(new TrackList(testTracks));

        mockMvc.perform(get(URL + "?title=track&artist=artist"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tracks", hasSize(5)));
    }

    @Test
    void addTrack_Song_ReturnsTrack() throws Exception {
        Song s = testTracks.get(0);

        when(tracksService.addTrack(any(Song.class))).thenReturn(s);

        mockMvc.perform(post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(s)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title").value("track0"));
    }

    @Test
    void addTrack_Song_BadRequest() throws Exception {
        Song s = new Song("","","");
        when(tracksService.addTrack(any(Song.class))).thenReturn(s);
        mockMvc.perform(post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(s)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getTrack_id_returnsTrack() throws Exception {
        Song s = testTracks.get(0);
        s.setId(0L);
        when(tracksService.getTrackById(anyLong())).thenReturn(s);

        mockMvc.perform(get(URL + "/" + s.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title").value(s.getTitle()));
    }

    @Test
    void getTrack_id_NoContent() throws Exception {
        when(tracksService.getTrackById(anyLong())).thenReturn(null);

        mockMvc.perform(get(URL + "/1010101"))
                .andExpect(status().isNoContent());
    }

    @Test
    void updateTrack_id_returnsNewTrack() throws Exception {
        Song s = testTracks.get(0);
        s.setId(1213L);
        when(tracksService.updateTrackById(anyLong(), any(Song.class))).thenReturn(s);
        mockMvc.perform(put(URL + "/" + s.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(s)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(1213));
    }

    @Test
    void updateTrack_id_NoContent() throws Exception {
        Song s = testTracks.get(0);
        s.setId(1213L);
        when(tracksService.updateTrackById(anyLong(), any(Song.class))).thenReturn(null);
        mockMvc.perform(put(URL + "/1213")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(s)))
                .andExpect(status().isNoContent());
    }

    @Test
    void updateTrack_Id_BadRequestTitle() throws Exception {
        Song s = new Song(null, "artist", "album");
        s.setId(1L);
        when(tracksService.updateTrackById(anyLong(), any(Song.class))).thenReturn(s);
        mockMvc.perform(put(URL + "/" + s.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(s)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateTrack_Id_BadRequestTitle2() throws Exception {
        Song s = new Song("", "artist", "album");
        s.setId(1L);
        when(tracksService.updateTrackById(anyLong(), any(Song.class))).thenReturn(s);
        mockMvc.perform(put(URL + "/" + s.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(s)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateTrack_Id_BadRequestArtist() throws Exception {
        Song s = new Song("title", null, "album");
        s.setId(1L);
        when(tracksService.updateTrackById(anyLong(), any(Song.class))).thenReturn(s);
        mockMvc.perform(put(URL + "/" + s.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(s)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateTrack_Id_BadRequestArtist2() throws Exception {
        Song s = new Song("title", "", "album");
        s.setId(1L);
        when(tracksService.updateTrackById(anyLong(), any(Song.class))).thenReturn(s);
        mockMvc.perform(put(URL + "/" + s.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(s)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteTrack_id_Accepted() throws Exception {
        mockMvc.perform(delete(URL + "/101"))
                .andExpect(status().isAccepted());

        verify(tracksService).deleteTrack(anyLong());
    }

    @Test
    void deleteTrack_id_NoContent() throws Exception {
        doThrow(new SongNotFoundException()).when(tracksService).deleteTrack(anyLong());
        mockMvc.perform(delete(URL + "/101"))
                .andExpect(status().isNoContent());
    }

    public String toJSON(Object o) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(o);
    }

}
