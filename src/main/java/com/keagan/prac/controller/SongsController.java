package com.keagan.prac.controller;

import com.keagan.prac.model.Song;
import com.keagan.prac.model.SongNotFoundException;
import com.keagan.prac.model.TrackList;
import com.keagan.prac.service.TracksService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;

@RestController
@RequestMapping("/api/music")
public class SongsController {

    TracksService tracksService;

    public SongsController(TracksService tracksService) {
        this.tracksService = tracksService;
    }

    @GetMapping
    public ResponseEntity<TrackList> getTracks(@RequestParam(required = false, defaultValue = "") String title,
                                               @RequestParam(required = false, defaultValue = "") String artist) {
        TrackList trackList;
        if (title.equals("") && artist.equals("")) {
            trackList = tracksService.getTracks();
        } else {
            trackList = tracksService.getTracks(title, artist);
        }
        if (trackList == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(trackList);
    }

    @PostMapping
    public ResponseEntity<Song> addTrack(@RequestBody Song s) {
        if (isBadRequest(s)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(tracksService.addTrack(s));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Song> getTrackById(@PathVariable Long id) {
        Song foundSong = tracksService.getTrackById(id);
        if (foundSong == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(foundSong);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Song> updateTrackById(@PathVariable Long id,
                                                @RequestBody Song song) {
        if (isBadRequest(song)) {
            return ResponseEntity.badRequest().build();
        }
        Song foundSong = tracksService.updateTrackById(id, song);
        if (foundSong == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(foundSong);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Song> deleteTrackById(@PathVariable Long id) {
        try {
            tracksService.deleteTrack(id);
        } catch (SongNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }

    private boolean isBadRequest(Song song) {
        if (song.getTitle() == null || song.getArtist() == null
                || song.getTitle().equals("") || song.getArtist().equals("")) {
            return true;
        }
        return false;
    }
}
